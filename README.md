# 🤖🖼️ Smart Graphics

Smart Graphics are programs that generate computer imagery using artificial intelligence algorithms.

* **AI-Based Rendering**: Intelligently rendered graphics
* **Language-Interactive**: Build applications by speaking to the computer using a human-spoken language like English and Chinese
* **Write Once, Run Anywhere**: The AI considers the device capabilities when rendering imagery


## 📝 Examples
This repository serves to introduce the notion of Smart Graphics as a term for communicating about AI algorithms that render computer imagery.

Open source examples of such programs are linked below for the community to learn from:

* A simple video game rendering AI
* A simple movie rendering AI
* Eye gaze warping using DeepWarp [demo](https://alteredqualia.com/xg/examples/eyes_gaze3.html), [code]()
* Changing Sketches into Photorealistic Masterpieces [video](https://www.youtube.com/watch?v=p5U4NgVGAwg) [code](https://github.com/NVlabs/SPADE)
* Changing Sketches into Photorealistic Masterpieces 2 [demo](http://gandissect.res.ibm.com/ganpaint.html?project=churchoutdoor&layer=layer4) [tutorial](https://gandissect.csail.mit.edu/) [code](https://github.com/CSAILVision/GANDissect)
* Image-to-image translation [demo](https://affinelayer.com/pixsrv/) [tutorial](https://phillipi.github.io/pix2pix/) [code](https://github.com/phillipi/pix2pix)

## 📜 Introduction

Smart Graphics are programs that generate computer imagery using artificial intelligence algorithms.

Artificial intelligence algorithms can be trained to generate images of cats, dogs and other animals in general (ie. using Generative Adversarial Networks aka GANs). Such a program is considered as a smart graphics program.

Smart Graphics are essentially a dynamic rendering pipeline. As opposed to a static rendering pipeline that consists of predefined functions called by a programmer (ie. a function to render a polygon).

Smart Graphics expose functions that can dynamically change their internal operations according to player input (ie. a function to render a cat).

Smart Graphics, by their nature, are interactive. They are interacteable whether by the programmer that trained the AI or by an end user who commands the AI to adapt to a new responsibility (ie. a person gives the command to render an octopus which perhaps the AI hasn't been trained to do; however, it was trained to know how to adapt to unforeseen requests).

## 👓 Use cases
- Offline-first Web browser
- Video games
- Movies
- "Her"-like operating system

**Smart Graphics** can apply to a great variety of use cases. One use case is a **web browser** that doesn't require an internet connection. An AI program can be trained to render an entire website. At some point it needs to connect to the internet to receive updates of the world, but this isn't necessary if you're consuming static content like news articles and videos that don't regularly change over time. And because this is AI afterall, the program can learn your preferences and retrieve information that you would want to know about prior to being offline.

**Video games** are another use case for Smart Graphics programs. An AI program can be trained to render the view of the player as they move through a virtual world. This has been demonstrated by NVidia through their groundbreaking video [Research at NVidia: The First Interactive AI Rendered Virtual World](https://www.youtube.com/watch?v=ayPqjPekn7g)

The **hollywood movie "Her"** showcases an operating system that's controlled by an AI algorithm named Samantha. To achieve an algorithm that communicates with people through a human-spoken language like English suggests that such a program has the capacity to render images on the screen display on its own (as opposed to relying on hardcoded instructions by human programmers).

## Demo Gif

![AI-Rendered Video](https://media.giphy.com/media/9VnKc6Na7kzugYAQvn/giphy.gif)
